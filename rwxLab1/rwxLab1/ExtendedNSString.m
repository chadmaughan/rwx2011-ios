//
//  ExtendedNSString.m
//  rwxLab1
//
//  Created by Chad Maughan on 11/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ExtendedNSString.h"

@implementation NSString (ExtendedNSString)
-(NSString*) stringWithLength {
    return [NSString stringWithFormat:@"%@ : %lu", self, [self length]];
}
@end
