//
//  main.m
//  rwxLab1
//
//  Created by Chad Maughan on 11/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ExtendedNSString.h"

int main(int argc, char *argv[])
{
    id emptyObject = nil;

    // create an array
    //  nil is an empty object that tells the compiler the size of the array
    NSArray* names = [NSArray arrayWithObjects: @"Bryan", @"Cory", nil];

    // loop through
    for (NSInteger i = 0; i < [names count]; i++) {
        NSLog(@"%@", [names objectAtIndex:i]);
    }
    
    // loop through with a category
    for (NSInteger i = 0; i < [names count]; i++) {
        NSLog(@"%@", [[names objectAtIndex:i] stringWithLength]);
    }

    NSLog(@")
    //  collections are immutable, if you want it to change, you have to explicitly use a mutable collection 
    NSMutableArray* mutableNames = [NSMutableArray arrayWithObjects:@"Jennie", @"Ethan", "@Jude", nil];
    [mutableNames addObject:@"Anson"];
    
    for(int i = 0; i < [mutableNames count]; i++) {
        NSLog(@"%@", [mutableNames objectAtIndex:i]);
    }

    return NSApplicationMain(argc, (const char **)argv);
}
