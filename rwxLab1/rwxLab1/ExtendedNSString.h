//
//  ExtendedNSString.h
//  rwxLab1
//
//  Created by Chad Maughan on 11/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ExtendedNSString)
-(NSString*) stringWithLength;
@end
