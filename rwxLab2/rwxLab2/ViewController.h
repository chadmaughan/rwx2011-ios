//
//  ViewController.h
//  rwxLab2
//
//  Created by Chad Maughan on 11/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

// the generated random number they'll look at
@property NSInteger magicNumber; 

// doesn't have to go in the header
- (int) generateRandomNumber;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UILabel *numberGuessLabel;
- (IBAction) guessButtonAction;
@end
