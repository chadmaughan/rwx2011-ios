//
//  ViewController.m
//  rwxLab2
//
//  Created by Chad Maughan on 11/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize numberTextField;
@synthesize numberGuessLabel;

@synthesize magicNumber;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

// this can't go on the bottom, like c++
- (int)generateRandomNumber {
    sranddev();
    return rand() % 100;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // what the instructor did
/*    [self setTargetValue: [self generateRandomNumber]];
  */  
    magicNumber = [self generateRandomNumber];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setNumberTextField:nil];
    [self setNumberGuessLabel:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)guessButtonAction {
    
    int guessedValue = [[numberTextField text] intValue];

    // sets the placeholder text (greyed out stuff) to what they entered in on the previous guess
    [numberTextField setPlaceholder:[numberTextField text]];

    if(guessedValue > magicNumber) {
        [numberGuessLabel setText:@"Too high. Try again."];
        [numberTextField setText:@""];
    }
    else if(guessedValue < magicNumber) {
        [numberGuessLabel setText:@"Too low. Try again."];
        [numberTextField setText:@""];        
    }
    else {
        [numberGuessLabel setText:@"You got it! Guess Again?"];
        
        magicNumber = [self generateRandomNumber];
    }
    
    [numberTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
