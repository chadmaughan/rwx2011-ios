#import <Foundation/Foundation.h>


@interface PalindromeChecker : NSObject {

}

- (Boolean)isPalindrome:(NSString *) inputString;

@end
