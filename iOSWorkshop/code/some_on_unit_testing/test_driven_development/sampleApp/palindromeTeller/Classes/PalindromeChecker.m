#import "PalindromeChecker.h"


@implementation PalindromeChecker

- (Boolean)isPalindrome:(NSString *) inputString {
	NSString *whiteSpaceStripped = [inputString stringByReplacingOccurrencesOfString:@" " withString:@""];
	NSString *lowerCaseString = [whiteSpaceStripped lowercaseString];
	NSMutableString *reversedString = [[NSMutableString alloc] initWithCapacity: lowerCaseString.length];
	
	for (int i = 0; i < lowerCaseString.length; i++) {
		int index = lowerCaseString.length - 1 - i;
		[reversedString appendFormat: @"%C", [lowerCaseString characterAtIndex: index]];
	}
	
	return [lowerCaseString isEqual: reversedString];
}

@end
