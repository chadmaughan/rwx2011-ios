#import "palindromeTellerViewController.h"
#import "PalindromeChecker.h"

@implementation palindromeTellerViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}

- (IBAction) checkPalindrome: (id) sender {
	NSString *inputString = inputStringField.text;
	NSString *yesOrNo = @"is not a palindrome";

	PalindromeChecker *checker = [PalindromeChecker alloc];
	
	if ([checker isPalindrome: inputString]) {
		yesOrNo = @"is a palindrome";
	}
	
	NSString *message = [[NSString alloc] initWithFormat:@"%@ %@", inputString, yesOrNo];
	palindromeMessage.text = message;
	[message release];
	[checker release];
	inputStringField.text = @"";	
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
