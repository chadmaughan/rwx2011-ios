#import <UIKit/UIKit.h>

@class palindromeTellerViewController;

@interface palindromeTellerAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    palindromeTellerViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet palindromeTellerViewController *viewController;

@end

