#import <UIKit/UIKit.h>

@interface palindromeTellerViewController : UIViewController {
	IBOutlet UILabel *palindromeMessage;
	IBOutlet UITextField *inputStringField;
}

- (IBAction) checkPalindrome: (id) sender;
@end

