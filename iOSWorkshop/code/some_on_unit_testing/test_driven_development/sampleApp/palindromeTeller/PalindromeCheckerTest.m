#import "PalindromeCheckerTest.h"
#import "Classes/PalindromeChecker.h"


@implementation PalindromeCheckerTest

- (void) testCanary {
    STAssertTrue(TRUE, @"Should pass");
}

- (void) testPalindromemom {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"mom"];
	
	[checker release];
	
	STAssertTrue(result, @"mom is a palindrome");
}

- (void) testPalindromeracecar {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"racecar"];
	
	[checker release];
	
	STAssertTrue(result, @"racecar is a palindrome");
}

- (void) testNotPalindromeracecars {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"racecars"];
	
	[checker release];
	
	STAssertFalse(result, @"racecars is not a palindrome");
}

- (void) testPalindromeRacecar {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"Racecar"];
	
	[checker release];
	
	STAssertTrue(result, @"Racecar is a palindrome");
}

- (void) testPalindromeLeadingSpace {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @" racecar"];
	
	[checker release];
	
	STAssertTrue(result, @" racecar is a palindrome");
}

- (void) testPalindromeTrilingSpace {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"racecar "];
	
	[checker release];
	
	STAssertTrue(result, @"racecar -is a palindrome");
}

- (void) testPalindromeLeadingAndTrailingSpace {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @" racecar "];
	
	[checker release];
	
	STAssertTrue(result, @" racecar -is a palindrome");
}

- (void) testPalindromeSpaceInBetween {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"race car"];
	
	[checker release];
	
	STAssertTrue(result, @"race car is a palindrome with space removed.");
}

- (void) testPalindromeAllowPalindromeSentences {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"Noel sees Leon"];
	
	[checker release];
	
	STAssertTrue(result, @"Noel sees Leon is a palindrome");
}

- (void) testPalindromeAllowPalindromeSentencesWithMultileSpaces {
	PalindromeChecker *checker = [PalindromeChecker new];
	
	Boolean result = [checker isPalindrome: @"Noel  sees Leon"];
	
	[checker release];
	
	STAssertTrue(result, @"Noel  sees Leon is a palindrome");
}

@end
