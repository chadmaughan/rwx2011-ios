#import "LocateMeViewController.h"

@implementation LocateMeViewController

@synthesize locationDisplayTextView;
@synthesize locationManager;
@synthesize startingLocation;
@synthesize currentLocation;

- (void)viewDidLoad {
  self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = 
     kCLLocationAccuracyBestForNavigation;
	[self.locationManager startUpdatingLocation];
  [super viewDidLoad];
}

-(void) locationManager:(CLLocationManager *)manager
       didFailWithError: (NSError *)error {	
	NSString* msg = @"User did not permit finding location";
	if ([error code] != kCLErrorDenied) msg = @"Failed to get location";
	self.locationDisplayTextView.text = 
    [NSString stringWithFormat:
      @"OOPS, something went wrong %@", msg];
}

-(void) locationManager:(CLLocationManager *)manager 
    didUpdateToLocation: (CLLocation *)newLocation 
    fromLocation:(CLLocation *)oldLocation {
	
  float factor = 3.2808;
  if (self.startingLocation == nil) self.startingLocation = newLocation;
  self.currentLocation = newLocation;
	NSString* msg = [NSString stringWithFormat: 
      @"Your location is %g°, %g°\n"
      "Accuracy %gm (%gft)\n"
      "Altitude %gm (%gft)\nAccuracy %gm\n"
      "you've traveled %gm (%gft)",
      newLocation.coordinate.latitude, 
      newLocation.coordinate.longitude,
      newLocation.horizontalAccuracy,
      newLocation.horizontalAccuracy * factor,
      newLocation.altitude, newLocation.altitude * factor,
      newLocation.verticalAccuracy,
      [newLocation distanceFromLocation: startingLocation],
      [newLocation distanceFromLocation: startingLocation] * factor];
	
  self.locationDisplayTextView.text = msg;
}

-(IBAction) showOnMap {
	if (!self.currentLocation) return;
	int zoom = 10;
	NSString* urlString = [NSString stringWithFormat: 
    @"http://map.google.com/maps?q=%@@%1.6f,%1.6f&z=%d",
    @"Your-Location",
    self.currentLocation.coordinate.latitude,
    self.currentLocation.coordinate.longitude,
    zoom];
	NSURL* url = [NSURL URLWithString: urlString];
	[[UIApplication sharedApplication] openURL: url];  
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
  self.locationDisplayTextView = nil;
  self.locationManager = nil;
  self.startingLocation = nil;
  self.currentLocation = nil;
  [super viewDidUnload];
}


- (void)dealloc {
  self.locationDisplayTextView = nil;
  self.locationManager = nil;
  self.startingLocation = nil;
  self.currentLocation = nil;
  [super dealloc];
}

@end
