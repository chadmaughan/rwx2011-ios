#import <UIKit/UIKit.h>

@class LocateMeViewController;

@interface LocateMeAppDelegate : NSObject <UIApplicationDelegate> {

  UIWindow *window;

  LocateMeViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;


@property (nonatomic, retain) IBOutlet LocateMeViewController *viewController;

@end

