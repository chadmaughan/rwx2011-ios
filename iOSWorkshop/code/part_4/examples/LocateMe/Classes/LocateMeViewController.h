#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LocateMeViewController : 
  UIViewController <CLLocationManagerDelegate> {}

@property (nonatomic, retain) IBOutlet UITextView* locationDisplayTextView;
@property (nonatomic, retain) CLLocationManager* locationManager;
@property (nonatomic, retain) CLLocation* startingLocation;
@property (nonatomic, retain) CLLocation* currentLocation;

-(IBAction) showOnMap;

@end

