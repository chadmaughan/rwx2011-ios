//
//  TouchesAndTalkingToAppsAppDelegate.h
//  TouchesAndTalkingToApps
//
//  Created by VENKAT SUBRAMANIAM on 12/2/10.
//  Copyright 2010 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TouchesAndTalkingToAppsViewController;

@interface TouchesAndTalkingToAppsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    TouchesAndTalkingToAppsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet TouchesAndTalkingToAppsViewController *viewController;

@end

