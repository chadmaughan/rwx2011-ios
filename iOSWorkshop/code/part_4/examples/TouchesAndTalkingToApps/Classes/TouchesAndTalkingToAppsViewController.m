//
//  TouchesAndTalkingToAppsViewController.m
//  TouchesAndTalkingToApps
//
//  Created by VENKAT SUBRAMANIAM on 12/2/10.
//  Copyright 2010 Agile Developer, Inc. All rights reserved.
//

#import "TouchesAndTalkingToAppsViewController.h"

@implementation TouchesAndTalkingToAppsViewController

@synthesize urlTextField;

-(IBAction) goClicked {
	NSString* url = [urlTextField text];
	NSURL* nsURL = [NSURL URLWithString: url];
	[[UIApplication sharedApplication] openURL: nsURL];
}

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	NSLog(@"Number of touches %d", [[event allTouches] count]);
	UITouch* aTouch = [touches anyObject];
	NSLog(@"Tap count is %d", [aTouch tapCount]);
}

@end








