//
//  TouchesAndTalkingToAppsViewController.h
//  TouchesAndTalkingToApps
//
//  Created by VENKAT SUBRAMANIAM on 12/2/10.
//  Copyright 2010 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchesAndTalkingToAppsViewController : UIViewController<CLLocationManagerDelegate> {

}

@property (nonatomic, retain) IBOutlet UITextField* urlTextField;

-(IBAction) goClicked;

@end

