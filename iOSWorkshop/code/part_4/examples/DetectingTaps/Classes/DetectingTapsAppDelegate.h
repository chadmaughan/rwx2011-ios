#import <UIKit/UIKit.h>

@class DetectingTapsViewController;

@interface DetectingTapsAppDelegate : NSObject <UIApplicationDelegate> {

  UIWindow *window;

  DetectingTapsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;


@property (nonatomic, retain) IBOutlet DetectingTapsViewController *viewController;

@end

