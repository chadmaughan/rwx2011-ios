#import "DetectingTapsViewController.h"

@implementation DetectingTapsViewController

@synthesize displayTextView;
@synthesize msg;

-(void) displayInfo: (NSString*) message {
  self.displayTextView.text = [NSString 
    stringWithFormat: @"%@\nYou received %@", 
    self.displayTextView.text,
    message];
}

-(void) touchesEnded: (NSSet*) touches withEvent:(UIEvent*) event {
  [NSObject cancelPreviousPerformRequestsWithTarget: self selector:@selector(displayInfo:) object: msg];
  
  self.msg = [NSString stringWithFormat: @"click count %d",
          [[touches anyObject] tapCount]];

  [self performSelector:@selector(displayInfo:) withObject: msg afterDelay: 0.25f];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
  self.displayTextView = nil;
  [super viewDidUnload];
}


- (void)dealloc {
  self.displayTextView = nil;
  [super dealloc];
}

@end
