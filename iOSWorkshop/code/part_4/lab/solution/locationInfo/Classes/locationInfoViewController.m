#import "locationInfoViewController.h"

@implementation locationInfoViewController

@synthesize latlongDisplayLabel;
@synthesize locationDisplayLabel;
@synthesize locationManager;
@synthesize currentLocation;
@synthesize formattedAddress;

-(void) locationDetails {
	NSURL* url = [NSURL URLWithString: [NSString stringWithFormat: @"http://maps.google.com/maps/api/geocode/xml?latlng=%g,%g&sensor=true",
					self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude]];
	NSURLRequest* request = [NSURLRequest requestWithURL:url];
	
	NSURLResponse* response = nil;
	NSError* error = nil;
	NSData* data = 
	[NSURLConnection sendSynchronousRequest: request returningResponse: &response error: &error];
	
	if (error != nil) {
		self.locationDisplayLabel.text = [error localizedDescription];
	} else {
		NSXMLParser* parser = 
		[[[NSXMLParser alloc] initWithData: data] autorelease];
		
		parser.delegate = self;
		[parser parse];
	}
}

- (void)viewDidLoad {
	self.locationManager = [[[CLLocationManager alloc] init] autorelease];
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = 
	    kCLLocationAccuracyBestForNavigation;
	[self.locationManager startUpdatingLocation];
	[super viewDidLoad];
}

-(void) locationManager:(CLLocationManager *)manager
       didFailWithError: (NSError *)error {	
	NSString* msg = @"User did not permit finding location";
	if ([error code] != kCLErrorDenied) msg = @"Failed to get location";
	self.latlongDisplayLabel.text = msg;
}

-(void) locationManager:(CLLocationManager *)manager 
    didUpdateToLocation: (CLLocation *)newLocation 
		   fromLocation:(CLLocation *)oldLocation {
	
	self.currentLocation = newLocation;
	NSString* msg = [NSString stringWithFormat: 
					 @"lat, long: %g°, %g°",
					 currentLocation.coordinate.latitude, 
					 currentLocation.coordinate.longitude];
	
	self.latlongDisplayLabel.text = msg;

	[self locationDetails];
}

-(IBAction) showOnMap {
	if (!self.currentLocation) return;
	int zoom = 10;
	NSString* urlString = [NSString stringWithFormat: 
						   @"http://map.google.com/maps?q=%@@%1.6f,%1.6f&z=%d",
						   @"Your-Location",
						   self.currentLocation.coordinate.latitude,
						   self.currentLocation.coordinate.longitude,
						   zoom];
	NSURL* url = [NSURL URLWithString: urlString];
	[[UIApplication sharedApplication] openURL: url];  
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	if ([elementName isEqualToString: @"formatted_address"]) {
		self.formattedAddress = [NSMutableString stringWithCapacity: 100];
	}
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	if ([elementName isEqualToString: @"formatted_address"]) {
		self.locationDisplayLabel.text = [NSString stringWithFormat: @"%@", self.formattedAddress];
		self.formattedAddress = nil;
		parser.delegate = nil;
	}
}
	
-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	if (self.formattedAddress != nil) {
		[self.formattedAddress appendString: string];
	}
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	self.latlongDisplayLabel = nil;
	self.locationDisplayLabel = nil;
	self.locationManager = nil;
	self.currentLocation = nil;
	self.formattedAddress = nil;
	[self viewDidUnload];
}


- (void)dealloc {
	self.latlongDisplayLabel = nil;
	self.locationDisplayLabel = nil;
	self.locationManager = nil;
	self.currentLocation = nil;
	self.formattedAddress = nil;
	[super dealloc];
}

@end
