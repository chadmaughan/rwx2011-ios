#import <UIKit/UIKit.h>

@class locationInfoViewController;

@interface locationInfoAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    locationInfoViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet locationInfoViewController *viewController;

@end

