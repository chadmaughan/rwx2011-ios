#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface locationInfoViewController : UIViewController<CLLocationManagerDelegate, NSXMLParserDelegate> {}

@property (nonatomic, retain) IBOutlet UILabel* latlongDisplayLabel;
@property (nonatomic, retain) IBOutlet UILabel* locationDisplayLabel;
@property (nonatomic, retain) CLLocationManager* locationManager;
@property (nonatomic, retain) CLLocation* currentLocation;
@property (nonatomic, retain) NSMutableString* formattedAddress;

-(IBAction) showOnMap;

@end

