//
//  RootViewController.h
//  peopleAtWork
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITableViewController {

}

@property (nonatomic, retain) NSMutableArray* people;

@end
