//
//  EditPersonViewController.m
//  peopleAtWork
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "EditPersonViewController.h"
#import "Person.h"

@implementation EditPersonViewController

@synthesize firstNameTextField;
@synthesize lastNameTextField;
@synthesize people;
@synthesize index;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    self.firstNameTextField = nil;
    self.lastNameTextField = nil;
    self.people = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self.firstNameTextField setText: [[self.people objectAtIndex: self.index] firstName]];
	[self.lastNameTextField setText: [[self.people objectAtIndex: self.index] lastName]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) doneClicked {
	Person* person = [[[Person alloc] init] autorelease];
	[person setFirstName: [self.firstNameTextField text]];
	[person setLastName: [self.lastNameTextField text]];
	[self.people replaceObjectAtIndex: index withObject: person];
	[self dismissModalViewControllerAnimated: YES];
}

-(IBAction) cancelClicked {
	[self dismissModalViewControllerAnimated: YES];
}

@end
