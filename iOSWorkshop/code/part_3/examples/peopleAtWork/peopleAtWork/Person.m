//
//  Person.m
//  peopleAtWork
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "Person.h"


@implementation Person

@synthesize firstName;
@synthesize lastName;

-(void) dealloc {
    self.firstName = nil;
    self.lastName = nil;
}
@end
