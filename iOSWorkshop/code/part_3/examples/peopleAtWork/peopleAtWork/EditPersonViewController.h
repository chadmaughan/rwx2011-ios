//
//  EditPersonViewController.h
//  peopleAtWork
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EditPersonViewController : UIViewController {
    
}

@property (nonatomic, retain) IBOutlet UITextField* firstNameTextField;
@property (nonatomic, retain) IBOutlet UITextField* lastNameTextField;
@property (nonatomic) NSInteger index;
@property (nonatomic, retain) NSMutableArray* people;

-(IBAction) doneClicked;
-(IBAction) cancelClicked;

@end
