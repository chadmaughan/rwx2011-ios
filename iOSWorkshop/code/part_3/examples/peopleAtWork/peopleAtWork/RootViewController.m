//
//  RootViewController.m
//  peopleAtWork
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "RootViewController.h"
#import "Person.h"
#import "EditPersonViewController.h"

@implementation RootViewController

@synthesize people;

-(void) editButtonClicked {
	[self.tableView setEditing: !self.tableView.editing];
}

-(void) addButtonClicked {
	Person* aPerson = [[[Person alloc] init] autorelease];
	aPerson.firstName = @"FIRSTNAME";
	aPerson.lastName = @"LASTNAME";
	[self.people addObject: aPerson];
    
	NSIndexPath* anIndexPath = 
    [NSIndexPath indexPathForRow: [self.people count] - 1 inSection: 0];
	NSArray* indexPaths = [NSArray arrayWithObject: anIndexPath];
	[self.tableView insertRowsAtIndexPaths: indexPaths withRowAnimation: YES];
	[self.tableView scrollToRowAtIndexPath: anIndexPath 
                          atScrollPosition: UITableViewScrollPositionBottom animated: YES];
}

- (void)viewDidLoad {
	UIBarButtonItem* editButton = [[[UIBarButtonItem alloc] initWithTitle: @"Edit"
		style: UIBarButtonItemStylePlain target: self action: @selector(editButtonClicked)] autorelease];
	
	[self.navigationItem setLeftBarButtonItem: editButton];
    
	UIBarButtonItem* addButton = [[[UIBarButtonItem alloc] initWithTitle: @"Add"
                                                                   style: UIBarButtonItemStylePlain target: self action: @selector(addButtonClicked)] autorelease];
	
	[self.navigationItem setLeftBarButtonItem: editButton];
	[self.navigationItem setRightBarButtonItem: addButton];
	
	Person* seth = [[Person alloc] init];
	seth.firstName = @"James";
	seth.lastName = @"Bond";
	self.people = [NSMutableArray arrayWithObjects:
                   seth, nil];
    [super viewDidLoad];
}

/*
 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
 }
 */
- (void)viewDidAppear:(BOOL)animated {
	[self.tableView reloadData];
    [super viewDidAppear:animated];
}
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.people count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MyPeople";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleSubtitle 
                 reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType: UITableViewCellAccessoryDetailDisclosureButton];
    }
    
	
	// Configure the cell.
	[cell.textLabel 
     setText: [[self.people objectAtIndex: indexPath.row] firstName]];
	
	[cell.detailTextLabel 
     setText: [[self.people objectAtIndex: indexPath.row] lastName]];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
		[self.people removeObjectAtIndex: indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	[self.people exchangeObjectAtIndex: fromIndexPath.row withObjectAtIndex:
	 toIndexPath.row];
}


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
	/*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
	
	EditPersonViewController* editPersonViewController =
    [[[EditPersonViewController alloc] initWithNibName: @"EditPersonViewController" bundle: nil] autorelease];
    [editPersonViewController setIndex: indexPath.row];
	[editPersonViewController setPeople: self.people];
	[self presentModalViewController: editPersonViewController animated: YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end