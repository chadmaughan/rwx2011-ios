//
//  Person.h
//  peopleAtWork
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Person : NSObject {
    
}

@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;

-(void) dealloc;

@end
