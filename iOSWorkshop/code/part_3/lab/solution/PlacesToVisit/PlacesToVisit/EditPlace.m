//
//  EditPlace.m
//  PlacesToVisit
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "EditPlace.h"


@implementation EditPlace

@synthesize placeTextField;
@synthesize placeDetailTextField;
@synthesize places;
@synthesize placesDetails;
@synthesize index;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
	self.placeTextField = nil;
	self.placeDetailTextField = nil;
	self.places = nil;
	self.placesDetails = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self.placeTextField setText: [self.places objectAtIndex: self.index]];
	[self.placeDetailTextField setText: [self.placesDetails objectAtIndex: self.index]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	self.placeTextField = nil;
	self.placeDetailTextField = nil;
	self.places = nil;
	self.placesDetails = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) doneClicked {
	[self.places replaceObjectAtIndex: index withObject: [self.placeTextField text]];
	[self.placesDetails replaceObjectAtIndex: index withObject: [self.placeDetailTextField text]];
	[self dismissModalViewControllerAnimated: YES];
}

-(IBAction) cancelClicked {
	[self dismissModalViewControllerAnimated: YES];
}
@end
