//
//  EditPlace.h
//  PlacesToVisit
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EditPlace : UIViewController {
    
}

@property (nonatomic, retain) IBOutlet UITextField* placeTextField;
@property (nonatomic, retain) IBOutlet UITextField* placeDetailTextField;
@property (nonatomic, retain) NSMutableArray* places;
@property (nonatomic, retain) NSMutableArray* placesDetails;

@property (nonatomic) NSInteger index;

-(IBAction) doneClicked;
-(IBAction) cancelClicked;

@end
