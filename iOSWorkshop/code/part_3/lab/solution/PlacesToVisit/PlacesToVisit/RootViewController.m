//
//  RootViewController.m
//  PlacesToVisit
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "RootViewController.h"
#import "EditPlace.h"

@implementation RootViewController

@synthesize places;
@synthesize placesDetails;

-(void) addButtonClicked {
	[self.places addObject: @"A favorite place"];
	[self.placesDetails addObject: @"Details"];
    
	NSIndexPath* indexPath = [NSIndexPath indexPathForRow: [self.places count] - 1 inSection: 0];
	NSArray* indexPaths = [NSArray arrayWithObject: indexPath];
	[self.tableView insertRowsAtIndexPaths: indexPaths withRowAnimation: UITableViewRowAnimationBottom];
}

-(void) editButtonClicked {
	[self setEditing: !self.editing];
}

- (void)viewDidLoad {
	UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithTitle: @"Add" 
                                                                  style: UIBarButtonItemStylePlain target: self action: @selector(addButtonClicked)];
    
	UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithTitle: @"Edit" 
                                                                   style: UIBarButtonItemStylePlain target: self action: @selector(editButtonClicked)];
    
    [super.navigationItem setLeftBarButtonItem: addButton];
	[super.navigationItem setRightBarButtonItem: editButton];
	
	self.places = [NSMutableArray arrayWithObjects: @"Key West", @"Miami Beach", nil];
	self.placesDetails = [NSMutableArray arrayWithObjects: @"You can see Cuba", @"Party Beach", nil];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
	[self.tableView reloadData];
    [super viewWillAppear:animated];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.places count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		[cell setAccessoryType: UITableViewCellAccessoryDetailDisclosureButton];
    }
    
    [cell.textLabel setText: [self.places objectAtIndex: [indexPath row]]];
	[cell.detailTextLabel setText: [self.placesDetails objectAtIndex: [indexPath row]]];
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
		[self.places removeObjectAtIndex: indexPath.row];
		[self.placesDetails removeObjectAtIndex: indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	[self.places exchangeObjectAtIndex: fromIndexPath.row withObjectAtIndex: toIndexPath.row];
	[self.placesDetails exchangeObjectAtIndex: fromIndexPath.row withObjectAtIndex: toIndexPath.row];
}


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
	/*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
	
	EditPlace* editPlaceViewController = [[[EditPlace alloc] initWithNibName: @"EditPlace" bundle: nil] autorelease];
	[editPlaceViewController setIndex: indexPath.row];
	[editPlaceViewController setPlaces: self.places];
	[editPlaceViewController setPlacesDetails: self.placesDetails];
	[self.navigationController presentModalViewController: editPlaceViewController animated: YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	self.places = nil;
	self.placesDetails = nil;
}


- (void)dealloc {
	self.places = nil;
	self.placesDetails = nil;
    [super dealloc];
}


@end

