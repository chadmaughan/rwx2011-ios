//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController {
    
    UITextField *nameTextField;
    UILabel *greetLabel;
}
@property (nonatomic, retain) IBOutlet UITextField *nameTextField;
@property (nonatomic, retain) IBOutlet UILabel *greetLabel;
- (IBAction)greetAction:(id)sender;

@end
