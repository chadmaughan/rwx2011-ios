//
//  HelloWorldAppDelegate.h
//  HelloWorld
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HelloWorldViewController;

@interface HelloWorldAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet HelloWorldViewController *viewController;

@end
