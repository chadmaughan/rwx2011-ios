//
//  lab01AppDelegate.h
//  lab01
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class lab01ViewController;

@interface lab01AppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet lab01ViewController *viewController;

@end
