//
//  lab01ViewController.h
//  lab01
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface lab01ViewController : UIViewController {
    
    UITextField *guessTextField;
    UILabel *statusLabel;
    
    int targetNumber;
}
@property (nonatomic, retain) IBOutlet UITextField *guessTextField;
- (IBAction)guessAction:(id)sender;

@property (nonatomic, retain) IBOutlet UILabel *statusLabel;

@end
