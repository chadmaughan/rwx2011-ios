//
//  lab01ViewController.m
//  lab01
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "lab01ViewController.h"

@implementation lab01ViewController
@synthesize guessTextField;
@synthesize statusLabel;

-(int) generateRandomNumber {
    sranddev();
    return rand() % 100;    
}

- (void)dealloc
{
    [guessTextField release];
    [statusLabel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    targetNumber = [self generateRandomNumber];
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setGuessTextField:nil];
    [self setStatusLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)guessAction:(id)sender {
    NSString* guessText = [guessTextField text];
    int guess = [guessText intValue];
    
    [guessTextField setPlaceholder: guessText];
    [guessTextField setText: @""];
    
    if(guess < targetNumber) {
        [statusLabel setText: @"Too low, try again"];
    }
    if (guess > targetNumber) {
        [statusLabel setText: @"Too high, try again"];        
    }
    
    if (guess == targetNumber) {
        [self generateRandomNumber];
        [statusLabel setText: @"Got it! play again"];
    }
}
@end
