Create a App that provides a UI for ordering coffee.

1. Allow the user to select the temperature from a slider control (left is
110°F, right is 200°F, anywhere in between is a number in some increments).
2. User can click on a button and you present three buttons for Small,
Medium, or Large. Once the user selects this, display that information in a
label.
3. When user clicks on the Order button, display the temperature and size.