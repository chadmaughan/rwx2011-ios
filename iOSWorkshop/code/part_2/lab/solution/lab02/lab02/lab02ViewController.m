//
//  lab02ViewController.m
//  lab02
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "lab02ViewController.h"

@implementation lab02ViewController

@synthesize temperatureSlider;
@synthesize coffeeSize;

- (void)dealloc
{
	self.temperatureSlider = nil;
	self.coffeeSize = nil;

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) changeSizeButtonClicked {
	[[[[UIActionSheet alloc] initWithTitle: @"Select Size"
                                  delegate: self 
                         cancelButtonTitle: @"Cancel"
                    destructiveButtonTitle: nil 
                         otherButtonTitles: @"Small", @"Medium", @"Large", nil] 
      autorelease] 
	 showInView: self.view];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
			[self.coffeeSize setText: @"Small"];
			break;
		case 1:
			[self.coffeeSize setText: @"Medium"];
			break;
		case 2:
			[self.coffeeSize setText: @"Large"];
			break;
		default:
			break;
	}
}

-(IBAction) orderButtonClicked {
	NSString* message = [NSString stringWithFormat: @"Let's get you %@ coffee at %3.0f°F",
                         [self.coffeeSize text], [self.temperatureSlider value]];
	
	[[[[UIAlertView alloc] 
       initWithTitle: @"Placing Order" 
       message: message 
       delegate: nil 
       cancelButtonTitle: @"OK" 
       otherButtonTitles: nil] 
      autorelease] 
	 show];
}

@end
