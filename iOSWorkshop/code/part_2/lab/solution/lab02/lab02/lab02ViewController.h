//
//  lab02ViewController.h
//  lab02
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface lab02ViewController : UIViewController<UIActionSheetDelegate> {
    
}

@property (nonatomic, retain) IBOutlet UISlider* temperatureSlider;
@property (nonatomic, retain) IBOutlet UILabel* coffeeSize;

-(IBAction) changeSizeButtonClicked;
-(IBAction) orderButtonClicked;

@end
