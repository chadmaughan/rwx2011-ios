//
//  workingWithEventsViewController.h
//  workingWithEvents
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface workingWithEventsViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate> {
    
    UILabel *messageLabel;
    UITextField *myTextField;
}
- (IBAction)orderAction:(id)sender;
- (IBAction)askAction:(id)sender;
@property (nonatomic, retain) IBOutlet UILabel *messageLabel;
@property (nonatomic, retain) IBOutlet UITextField *myTextField;

@property (nonatomic, retain) NSArray* optionsForActionSheet;

@end
