//
//  workingWithEventsViewController.m
//  workingWithEvents
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "workingWithEventsViewController.h"

@implementation workingWithEventsViewController
@synthesize messageLabel;
@synthesize myTextField;
@synthesize optionsForActionSheet;

- (void)dealloc
{
    [messageLabel release];
    [myTextField release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [myTextField setDelegate: self];
    
    [self setOptionsForActionSheet:
    [NSArray arrayWithObjects:@"Small", @"Medium", @"Large", nil]];
    
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setMessageLabel:nil];
    [self setMyTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"handled...");
    [messageLabel setText: [myTextField text]];
    [myTextField resignFirstResponder];
    
    return true;
}

- (IBAction)orderAction:(id)sender {
    UIActionSheet* sheet = [[[UIActionSheet alloc] initWithTitle:@"Select your order" delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle: [self.optionsForActionSheet objectAtIndex: 0] otherButtonTitles:[self.optionsForActionSheet objectAtIndex: 1], [self.optionsForActionSheet objectAtIndex: 2], nil] autorelease];
    
    [sheet showInView:self.view];
}

- (IBAction)askAction:(id)sender {
    UIAlertView* alertView = [[[UIAlertView alloc] initWithTitle:@"Are you sure" message: @"Are you really sure" delegate: self cancelButtonTitle:@"Nope" otherButtonTitles: @"maybe", nil] autorelease];
    
    [alertView show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"You clicked button at index %d", buttonIndex);
}

-(void) alertViewCancel:(UIAlertView *)alertView {
    NSLog(@"You cancelled...");
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString* selection = [NSString stringWithFormat:@"%@", [optionsForActionSheet objectAtIndex: buttonIndex]];
    [messageLabel setText: selection];
}

@end
