//
//  main.m
//  lab0
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyStringUtil.h"

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];


    NSArray* names = [NSArray arrayWithObjects: @"Ben", @"Fred", @"Joe", nil];
    
    for(int i = 0; i < [names count]; i++) {
        NSLog([[names objectAtIndex: i] stringWithLength]);
    }
    
    [pool drain];
    return 0;
}

