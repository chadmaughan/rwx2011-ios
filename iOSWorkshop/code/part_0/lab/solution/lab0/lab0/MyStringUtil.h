//
//  MyStringUtil.h
//  lab0
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(MyStringUtil)

-(NSString*) stringWithLength;

@end
