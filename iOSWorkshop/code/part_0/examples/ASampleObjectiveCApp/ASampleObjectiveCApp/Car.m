//
//  Car.m
//  ASampleObjectiveCApp
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "Car.h"


@implementation Car

-(int) miles {
    NSLog(@"miles called");
    return 0;
}

-(void) driveDistance:(int)distance atSpeed:(int)speed {
    NSLog(@"in drive method");
}

-(void) dealloc {
    NSLog(@"Deallocated car");
}

+(id) carWithZeroMiles {
    if((self = [[[Car alloc] init] autorelease]) != nil) {
        NSLog(@"created");
    }
    
    return self;
}

-(void) turn {
    NSLog(@"turning...");
}

-(void) stop {
    NSLog(@"stopping");
}

@end
