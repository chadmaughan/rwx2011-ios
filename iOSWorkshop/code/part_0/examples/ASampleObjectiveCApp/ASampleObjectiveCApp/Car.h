//
//  Car.h
//  ASampleObjectiveCApp
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Drivable

-(void) turn;

@optional 
-(void) reverse;

@required 
-(void) stop;

@end

@interface Car : NSObject<Drivable> {
    
}

-(int) miles;

-(void) driveDistance: (int) distance atSpeed: (int) speed;

-(void) dealloc;

+(id) carWithZeroMiles;

-(void) turn;

-(void) stop;

@end
