//
//  main.m
//  ASampleObjectiveCApp
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "VenkatsStringUtil.h"

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    // insert code here...
    NSLog(@"Hello, World!");

    NSString* name = @"Sam";
    NSString* greet = [NSString stringWithFormat: @"Hello, %@ is %d years old", name, 10];
    
    Car* car1 = [[Car alloc] init];
    
    int theMiles = [car1 miles];
    
    [car1 driveDistance: 120 atSpeed: 60];
    
    [car1 release];
    
    Car* car2 = [[[Car alloc] init] autorelease];
    [car2 miles];
    // no need to release this.
    
    Car* car3 = [Car carWithZeroMiles]; //This is preferred.
    [car3 miles];
    
    //Another way to call a method
    SEL theMilesMethod = @selector(miles);
    
    [car3 performSelector: theMilesMethod];
    
    [car3 turn];
    
    id<Drivable> drivable = car3;
    [drivable stop];
    
    
    NSString* stop = @"stop";
    
    NSLog([stop shout]);
    
    NSArray* names = [NSArray arrayWithObjects: @"Ben", @"Neal", @"Fred", nil];
    
    for(int i = 0; i < [names count]; i++) {
        NSLog([names objectAtIndex: i]);
    }
    
    [pool drain];
    return 0;
}

