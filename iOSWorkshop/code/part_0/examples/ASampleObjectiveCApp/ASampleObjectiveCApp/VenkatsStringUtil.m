//
//  VenkatsStringUtil.m
//  ASampleObjectiveCApp
//
//  Created by Venkat Subramaniam on 7/12/11.
//  Copyright 2011 Agile Developer, Inc. All rights reserved.
//

#import "VenkatsStringUtil.h"


@implementation NSString(VenkatsStringUtil)

-(NSString*) shout {
    return [self uppercaseString];
}

@end
